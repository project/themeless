# Themeless #

Themeless node output on path 'node/%/themeless' and 'some/alias/themeless'.


## INSTALLATION ##

- Enable the module at ../admin/modules.


## GET STARTED ##

- Enable the Themeless display mode per content type under the 'Default' view
  mode in the section SHOW CUSTOM DISPLAY SETTINGS at
  'admin/structure/types/manage/[your-content-type]/display'.
- Select the format at the bottom of
  'admin/structure/types/manage/[your-content-type]/display/themeless'.
- Choose the desired fields and field settings moving them from 'Hidden'.
- Save.
- Visit a content page at ../node/%nid/themeless or ../some/alias/themeless.


## TO KEEP IN MIND ##

- Works currently only for nodes.
- Works with fields that output as text (for example also select) and a
  tablefield (https://www.drupal.org/project/tablefield).
- A tablefield needs to be configured with a corresponding JSON, XML or HTML
  format at /admin/structure/types/manage/[your-content-type]/display/themeless.
- URLs with aliases are accessible for the themeless output,
  e.g. ../my/custom/alias/themeless.
- Add 'Disallow: */themeless' in the robots.txt file found in the root of the
  Drupal install. This should avoid these pages being indexed by Search Engines.
