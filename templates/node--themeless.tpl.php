<?php

/**
 * @file
 * Themeless theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php
  // We hide the comments and links.
  hide($content['comments']);
  hide($content['links']);
  print render($content);
?>
